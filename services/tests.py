from decimal import Decimal

from django.contrib.auth.models import User
from django.test import TestCase

from services.models import Service, ServiceWorker


class ServiceModelTests(TestCase):
    def setUp(self):
        User.objects.bulk_create([
            User(
                username='john_doe',
                first_name='John',
                last_name='Doe',
                email='john_doe@gmail.com',
                password='password'
            ),
            User(
                username='james_doe',
                first_name='James',
                last_name='Doe',
                email='james_doe@gmail.com',
                password='password'
            ),
            User(
                username='jane_doe',
                first_name='Jane',
                last_name='Doe',
                email='jane_doe@gmail.com',
                password='password'
            ),
            User(
                username='judy_doe',
                first_name='Judy',
                last_name='Doe',
                email='judy_doe@gmail.com',
                password='password'
            )
        ])

        Service.objects.bulk_create([
            Service(
                name='cleaning home',
                price=499
            ),
            Service(
                name='electrical work',
                price=200
            ),
            Service(
                name='moving grass',
                price=522.50
            )
        ])

        service_1 = Service.objects.get(name='cleaning home')
        service_2 = Service.objects.get(name='electrical work')
        service_3 = Service.objects.get(name='moving grass')

        worker_1 = User.objects.get(email='john_doe@gmail.com')
        worker_2 = User.objects.get(email='james_doe@gmail.com')
        worker_3 = User.objects.get(email='jane_doe@gmail.com')
        worker_4 = User.objects.get(email='judy_doe@gmail.com')

        ServiceWorker.objects.bulk_create([
            ServiceWorker(
                service=service_1,
                worker=worker_3
            ),
            ServiceWorker(
                service=service_1,
                worker=worker_4
            ),
            ServiceWorker(
                service=service_2,
                worker=worker_1
            ),
            ServiceWorker(
                service=service_2,
                worker=worker_2
            ),
            ServiceWorker(
                service=service_3,
                worker=worker_2
            )
        ])

    def test_get_common_price_sum(self):
        """Should return common price selected services."""
        self.assertEqual(
            Service.objects.get_common_price(),
            Decimal('1221.50')
        )

    def test_get_common_price_type(self):
        """Should return decimal type value."""
        self.assertEqual(
            type(Service.objects.get_common_price()),
            type(Decimal())
        )
