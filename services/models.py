from decimal import Decimal

from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Sum


class ServiceQuerySet(models.QuerySet):
    def get_common_price(self) -> Decimal:
        """Returns common price services."""
        return self.aggregate(common_price=Sum('price'))['common_price']


class Service(models.Model):
    CURRENCY = '$'

    workers = models.ManyToManyField(
        User,
        through='ServiceWorker',
        related_name='worker_services'
    )
    name = models.CharField(max_length=255)
    price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        validators=[MinValueValidator(Decimal('0.01'))]
    )
    currency = models.CharField(max_length=10, default=CURRENCY)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = ServiceQuerySet.as_manager()

    def __str__(self):
        return self.name


class ServiceWorker(models.Model):
    class Meta:
        db_table = 'services_service_workers'

    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    worker = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
