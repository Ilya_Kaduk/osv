from django.conf import settings
from django.contrib import admin

from services.models import Service, ServiceWorker


class ServiceWorkerInline(admin.TabularInline):
    model = ServiceWorker
    extra = 1


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'currency')
    readonly_fields = ('currency',)
    list_per_page = settings.LIST_PER_PAGE
    inlines = [ServiceWorkerInline]

    class Meta:
        ordering = ['-pk']


admin.site.register(Service, ServiceAdmin)
