$('#order-form').on('submit', function(event){
    event.preventDefault();

    $.ajax({
        url: $(this).attr('action'),
        data: $(this).serialize(),
        method: 'POST',
        dataType: 'json',
        success: function(response) {
            $('#order-form').find('.order-form__input').prop('checked', false)

            if (response['status'] == 'SUCCESS') {
                UIkit.notification(
                    "<span uk-icon='icon: check'></span> Success! Order processing...",
                    {status: 'success', pos: 'top-center'}
                );
            }
        },
        error: function(response) {
            var error_fields = response.responseJSON;
            for (error_field in error_fields) {
                var error_messages = error_fields[error_field];
                for (error_message of error_messages) {
                    UIkit.notification(
                        "<span uk-icon='icon: check'></span> " + error_message,
                        {status: 'danger', pos: 'top-center'}
                    );
                }
            }
        },
        statusCode: {
            500: function() {
                error_message = 'Oops! Something went wrong.'
                UIkit.notification(
                    "<span uk-icon='icon: check'></span> " + error_message,
                    {status: 'danger', pos: 'top-center'}
                );
            }
        }
    });
});