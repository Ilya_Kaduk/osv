from abc import ABC, abstractmethod

import logging
from django.utils import timezone

logger = logging.getLogger('order')


class OrderLogUtil(ABC):
    @abstractmethod
    def log_order(self, request, form) -> None:
        pass


class OrderFileLogUtil(OrderLogUtil):
    def log_order(self, request, form) -> None:
        """Adds order details to log."""
        order_details = self.prepare_order_details(
            customer_name=request.user.username,
            service_list=[
                service.name for service in form.cleaned_data['services']
            ]
        )
        logger.info(order_details)

    def prepare_order_details(self, customer_name: str, service_list: list) -> str:
        """Returns details about an order before create."""
        return '{customer_name} ~ {date_time} ~ {service_list}'.format(
            customer_name=customer_name,
            service_list=', '.join(service_list),
            date_time=timezone.now().replace(microsecond=0)
        )
