from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse

from orders.utils import OrderFileLogUtil
from orders.tasks import process_order


class OrderResponseMixin:
    order_log_util_class = OrderFileLogUtil

    def is_ajax(self, request):
        """
        Checks if the request is an ajax. Do it because the is_ajax function
        deprecated for django 4.2.
        """
        return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.is_ajax(self.request):
            return JsonResponse(
                form.errors,
                status=HttpResponseBadRequest.status_code
            )
        return response

    def form_valid(self, form):
        order_log_util = self.order_log_util_class()
        order_log_util.log_order(self.request, form)

        form.cleaned_data['customer'] = self.request.user
        response = super().form_valid(form)

        process_order.apply_async(kwargs={'order_pk': self.object.pk})

        if self.is_ajax(self.request):
            return JsonResponse(
                data={'status': 'SUCCESS'},
                status=HttpResponse.status_code
            )
        return response
