from django.utils import timezone
from django.contrib.auth.models import User
from django.test import TestCase

from orders.models import Order, OrderServiceWorker
from orders.utils import OrderFileLogUtil
from services.models import Service, ServiceWorker


class OrderFileLogUtilTests(TestCase):
    order_file_log_util = OrderFileLogUtil()

    def test_prepare_order_details_success(self):
        """Should return details about an order."""
        order_details = self.order_file_log_util.prepare_order_details(
            customer_name='Ilya',
            service_list=['Plumbing work', 'Moving grass']
        )
        valid_order_details = \
            'Ilya ~ {date_time} ~ Plumbing work, Moving grass'.format(
                date_time=timezone.now().replace(microsecond=0)
            )
        self.assertEqual(order_details, valid_order_details)

    def test_prepare_order_details_empty_customer_name(self):
        """
        Should return details about an order without customer name.
        """
        order_details = self.order_file_log_util.prepare_order_details(
            customer_name='',
            service_list=['Plumbing work', 'Moving grass']
        )
        valid_order_details = \
            ' ~ {date_time} ~ Plumbing work, Moving grass'.format(
                date_time=timezone.now().replace(microsecond=0)
            )
        self.assertEqual(order_details, valid_order_details)

    def test_prepare_order_details_empty_service_list(self):
        """Should return details about an order without service list."""
        order_details = self.order_file_log_util.prepare_order_details(
            customer_name='Ilya',
            service_list=[]
        )
        valid_order_details = 'Ilya ~ {date_time} ~ '.format(
            date_time=timezone.now().replace(microsecond=0)
        )
        self.assertEqual(order_details, valid_order_details)


class OrderModelTests(TestCase):
    def setUp(self):
        User.objects.bulk_create([
            User(
                username='john_doe',
                first_name='John',
                last_name='Doe',
                email='john_doe@gmail.com',
                password='password'
            ),
            User(
                username='james_doe',
                first_name='James',
                last_name='Doe',
                email='james_doe@gmail.com',
                password='password'
            ),
            User(
                username='mike_doe',
                first_name='Mike',
                last_name='Doe',
                email='mike_doe@gmail.com',
                password='password'
            ),
            User(
                username='jim_doe',
                first_name='Jim',
                last_name='Doe',
                email='jim_doe@gmail.com',
                password='password'
            ),
            User(
                username='bill_doe',
                first_name='Bill',
                last_name='Doe',
                email='bill_doe@gmail.com',
                password='password'
            ),
            User(
                username='jane_doe',
                first_name='Jane',
                last_name='Doe',
                email='jane_doe@gmail.com',
                password='password'
            )
        ])

        Service.objects.bulk_create([
            Service(
                name='plumbing work',
                price=200
            ),
            Service(
                name='electrical work',
                price=300
            ),
            Service(
                name='moving grass',
                price=100
            )
        ])

        service_1 = Service.objects.get(name='plumbing work')
        service_2 = Service.objects.get(name='electrical work')
        service_3 = Service.objects.get(name='moving grass')

        worker_1 = User.objects.get(email='john_doe@gmail.com')
        worker_2 = User.objects.get(email='james_doe@gmail.com')
        worker_3 = User.objects.get(email='mike_doe@gmail.com')
        worker_4 = User.objects.get(email='jim_doe@gmail.com')
        worker_5 = User.objects.get(email='bill_doe@gmail.com')

        ServiceWorker.objects.bulk_create([
            ServiceWorker(
                service=service_1,
                worker=worker_1
            ),
            ServiceWorker(
                service=service_1,
                worker=worker_2
            ),
            ServiceWorker(
                service=service_2,
                worker=worker_2
            ),
            ServiceWorker(
                service=service_2,
                worker=worker_3
            ),
            ServiceWorker(
                service=service_2,
                worker=worker_4
            ),
            ServiceWorker(
                service=service_3,
                worker=worker_5
            )
        ])

        services = Service.objects.all()
        order = Order.objects.create(
            customer=User.objects.get(email='jane_doe@gmail.com'),
            price=services.get_common_price()
        )
        order_service_worker_list = [
            OrderServiceWorker(order=order, service=service)
            for service in services
        ]
        OrderServiceWorker.objects.bulk_create(order_service_worker_list)

    def test_assign_worker_success(self):
        """
        Must add a worker for each service on the order and change the status
        to "assigned".
        """
        order = Order.objects.get()
        self.assertIsInstance(order.assign_workers(), int)
        self.assertFalse(
            order.order_service_worker.filter(worker__isnull=True).exists()
        )
        self.assertEqual(
            order.order_service_worker.filter(
                status=OrderServiceWorker.STATUS_ASSIGNED
            ).count(),
            order.order_service_worker.count()
        )

    def test_assign_worker_service_has_no_workers(self):
        """
        If a service has no workers, they are not assigned to an order and
        the status remains as "processing".
        """
        service = Service.objects.first()
        service.workers.all().delete()
        order = Order.objects.get()
        order.assign_workers()
        self.assertTrue(
            order.order_service_worker.filter(worker__isnull=True).exists()
        )
        self.assertNotEqual(
            order.order_service_worker.filter(
                status=OrderServiceWorker.STATUS_ASSIGNED
            ).count(),
            order.order_service_worker.count()
        )
        self.assertEqual(
            order.order_service_worker.get(service=service).worker_id,
            None
        )
