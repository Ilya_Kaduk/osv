from django.db import transaction
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from orders.models import Order
from orders.widgets import ServiceWidget


class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['services']
        widgets = {'services': ServiceWidget()}
        error_messages = {
            'services': {
                'required': _('Please select a service')
            }
        }

    def save(self, commit=True):
        order = super().save(commit=False)
        order.customer = self.cleaned_data['customer']

        if commit:
            with transaction.atomic():
                order.save()
                self.save_m2m()
        return order
