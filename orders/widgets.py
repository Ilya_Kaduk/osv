from django.forms import CheckboxSelectMultiple


class ServiceWidget(CheckboxSelectMultiple):
    template_name = 'orders/order_form_services.html'

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['services'] = self.choices.queryset
        return context
