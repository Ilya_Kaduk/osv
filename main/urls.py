from django.contrib.auth import views
from django.urls import path

urlpatterns = [
    path(
        'auth/login/',
        views.LoginView.as_view(
            template_name='main/auth/login.html',
            redirect_authenticated_user=True
        ),
        name='login'
    ),
    path('auth/logout/', views.LogoutView.as_view(), name='logout'),
]
