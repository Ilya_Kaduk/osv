# Osv

Osv is a system that allows you to create services, assign workers to them
and create orders for those services.

## Requirements

Ubuntu 20.04

Python 3.9.10

PostgreSQL 14.3

RabbitMQ 3.8.2

Node.js 18.14.1

Npm 9.3.1

## Installation

Create and activate virtualenv.

Clone project.

Go to project dir and install requirements. You can set specific requirements for each server.

```
pip3 install -r requirements/local.txt

npm i
```

## Work with configs

For the configs to work correctly in various servers, you must explicitly 
specify which configs you are using.

```
./manage.py runserver --settings=config.local
```

By default, the configs from the config/base.py file are used.

## Configs

1. Create .env file in root dir and add your specific settings. See env_example.txt

2. Run migrations and create a superuser. This can be done for various servers.

```
./manage.py migrate --settings=config.local

./manage.py createsuperuser --settings=config.local
```

## Run

```
./manage.py runserver --settings=config.local

celery -A config worker -l info -Q default,order
```

## Run tests

```
./manage.py test --settings=config.local
```

## How it works?

Login to admin panel.

Create workers.

Create services and assign workers for each service.

Go to home page http://127.0.0.1:8000 and login.

Select services and create order.

System will process order and assign worker for each selected services.